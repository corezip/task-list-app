const express = require('express');
const Tasks = require('../models/Task');
const Files = require('../models/File');
const multer = require('multer');
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'storage/');
  },
  filename: (req, file, cb) => {
    // const extArray = file.mimetype.split('/');
    // const extension = extArray[extArray.length - 1];
    cb(null, file.originalname);
  },
});
const upload = multer({storage: storage});
// const upload = multer({dest: 'storage/'});
// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const task = req.query.task;
    const order = req.query.orderBy;
    const startDate = req.query.startDate;
    const endDate = req.query.endDate;
    const result = await Tasks
        .query()
        .where('userID', id)
        .eager('[status, files]')
        .modify((queryExtra) => {
          if (task) {
            queryExtra.where('task', task);
          }
          if (order) {
            queryExtra.orderBy(order);
          }
          if (startDate && endDate) {
            queryExtra.whereBetween('task_datetime', [startDate, endDate]);
          }
        });
    res.status(200).send(result);
  } catch (er) {
    console.log(er);
    res.status(500).send({message: er.message});
  }
  return;
});


router.post('/', upload.any(), async (req, res) => {
  if (!req.body.task || !req.body.task_datetime ||
    !req.body.statusID || !req.body.userID) {
    res.status(400).send();
    return;
  }
  try {
    const newFiles = [];
    const newTask = await Tasks
        .query()
        .insert({
          task: req.body.task,
          description: req.body.description,
          task_datetime: req.body.task_datetime,
          statusID: parseInt(req.body.statusID, 10),
          userID: parseInt(req.body.userID, 10),
        });
    if (req.files) {
      for (let i = 0; req.files.length > i; i++) {
        const file = req.files[i];
        newFiles.push(await Files.query()
            .insert({
              path: file.path,
              taskID: newTask.id,
              file_name: file.filename,
            }));
      }
    }
    res.status(201).send({newTask, newFiles});
  } catch (er) {
    console.log(er);
    res.status(500).send({message: er.message});
  }
  return;
});

router.put('/', upload.any(), async (req, res) => {
  if (!req.body.task || !req.body.task_datetime ||
    !req.body.statusID || !req.body.userID || !req.body.id) {
    res.status(400).send();
    return;
  }
  const update = {
    task: req.body.task,
    description: req.body.description,
    task_datetime: req.body.task_datetime,
    statusID: parseInt(req.body.statusID, 10),
    userID: parseInt(req.body.userID, 10),
  };
  try {
    existe = await Tasks.query()
        .findById(parseInt(req.body.id, 10))
        .patch(update);
    if (existe == 0) {
      res.status(404).send();
    } else {
      if (req.files) {
        console.log(req.files);
      }
      res.status(200).send(update);
    }
  } catch (er) {
    console.log(er);
    res.status(500).send({message: er.message});
  }
  return;
});

router.delete('/', async (req, res) => {
  if (!req.body.id) {
    res.status(400).send();
    return;
  }
  const id = req.body.id;
  try {
    await Tasks
        .query()
        .deleteById(id);
    res.status(200).send({message: 'Delete Exitoso!'});
  } catch (er) {
    res.status(500).send({message: er.message});
  }
  return;
});

module.exports = router;
