const express = require('express');
const jwt = require('jsonwebtoken');
const {key} = require('../config/config');
const Users = require('../models/User');
const {encode} = require('../controller/index');
// eslint-disable-next-line new-cap
const router = express.Router();

router.post('/login', async (req, res) => {
  const username = req.body.user;
  const password = req.body.password;
  const exist = await Users
      .query()
      .where('username', username)
      .where('password', encode(password));
  if (!exist.length) {
    res.status(401).send({
      error: 'usuario o contraseña inválidos',
    });
    return;
  }
  const user = {
    username: username,
    id: exist[0].id,
  };
  const token = jwt.sign(user, key, {
    expiresIn: 60 * 60 * 2, // expires in 2 hours
  });
  res.send({
    token,
    user,
  });
});

router.get('/secure', (req, res) => {
  let token = req.headers.authorization;
  if (!token) {
    res.status(401).send({
      error: 'Es necesario el token de autenticación',
    });
    return;
  }
  token = token.replace('Bearer ', '');
  jwt.verify(token, key, (err, user) => {
    console.log(user);
    if (err) {
      res.status(401).send({
        error: 'Token inválido',
      });
    } else {
      res.send({
        message: 'Awwwww yeah!!!!',
      });
    }
  });
});

module.exports = router;
