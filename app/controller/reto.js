const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const buscame = req.body.buscame;
    const array = req.body.array;
    for (let resultado = 0; array.length > resultado; resultado++) {
      if (buscame == array[resultado] || buscame < array[resultado]) {
        res.status(200).send({resultado});
        break;
      }
    }
  } catch (er) {
    console.log(er);
    res.status(500).send({message: er.message});
  }
  return;
});

module.exports = router;
