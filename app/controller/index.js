const encode = (text) => {
  const buff = Buffer.from(text);
  return buff.toString('base64');
};

const decode = (textBase64) => {
  const buff = Buffer.from(textBase64, 'base64');
  return buff.toString('ascii');
};

module.exports = {
  encode,
  decode};
