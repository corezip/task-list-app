const express = require('express');
const Files = require('../models/File');
const fs = require('fs');
// eslint-disable-next-line new-cap
const router = express.Router();

router.post('/', async (req, res) => {
  if (!req.body.id) {
    res.status(400).send();
    return;
  }
  const id = req.body.id;
  try {
    const dataFile = await Files
        .query()
        .findById(id);
    const file = './' + dataFile.path;
    if (fs.existsSync('./' + dataFile.path)) {
      res.download(file);
    } else {
      res.status(404).end();
    }
  } catch (er) {
    console.log(er);
    res.status(500).send({message: er.message});
  }
  return;
});

router.delete('/', async (req, res) => {
  if (!req.body.id) {
    res.status(400).send();
    return;
  }
  const id = req.body.id;
  try {
    const dataFile = await Files
        .query()
        .findById(id);
    await Files
        .query()
        .deleteById(id);
    if (fs.existsSync('./' + dataFile.path)) {
      fs.unlinkSync('./' + dataFile.path);
    }
    res.status(200).send({message: 'Delete Exitoso!'});
  } catch (er) {
    res.status(500).send({message: er.message});
  }
  return;
});

module.exports = router;
