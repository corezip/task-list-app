const express = require('express');
const {encode, decode} = require('./index');
const Users = require('../models/User');
// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/:id', async (req, res) => {
  try {
    const user = await Users
        .query()
        .findById(req.params.id);
    user.password = decode(user.password);
    res.status(200).send(user);
  } catch (er) {
    console.log(er);
    res.status(500).send({message: er.message});
  }
  return;
});

router.post('/', async (req, res) => {
  if (!req.body.user || !req.body.email || !req.body.password) {
    res.status(400).send();
    return;
  }
  const user = req.body.user;
  const email = req.body.email;
  const password = encode(req.body.password);
  try {
    const newUser = await Users
        .query()
        .insert({username: user, email: email, password: password});
    res.status(201).send(newUser);
  } catch (er) {
    console.log(er);
    res.status(500).send({message: er.message});
  }
  return;
});

router.put('/', async (req, res) => {
  if (!req.body.user || !req.body.email || !req.body.password || !req.body.id) {
    res.status(400).send();
    return;
  }
  const id = req.body.id;
  const update = {
    username: req.body.user,
    email: req.body.email,
    password: encode(req.body.password),
  };
  try {
    existe = await Users.query()
        .findById(id)
        .patch(update);
    if (existe == 0) {
      res.status(404).send();
    } else {
      res.status(200).send(update);
    }
  } catch (er) {
    res.status(500).send({message: er.message});
  }
  return;
});

router.delete('/', async (req, res) => {
  if (!req.body.id) {
    res.status(400).send();
    return;
  }
  const id = req.body.id;
  try {
    await Users
        .query()
        .deleteById(id);
    res.status(200).send({message: 'Delete Exitoso!'});
  } catch (er) {
    res.status(500).send({message: er.message});
  }
  return;
});

module.exports = router;
