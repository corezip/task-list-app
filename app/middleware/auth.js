const jwt = require('jsonwebtoken');
const {key} = require('../config/config');

const isAuth = (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      res.status(403).send({
        message: 'No Autorizado',
      });
    }
    const token = req.headers.authorization;
    token = token.replace('Bearer ', '');
    jwt.verify(token, key, (err, payload) => {
      console.log(payload);
      if (payload) {
      } else {
        next();
      }
    });
  } catch (e) {
    next();
  }
};

module.exports = isAuth;
