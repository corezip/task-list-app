/* eslint-disable require-jsdoc */
const {Model} = require('objection');

class Status extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'status';
  };

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['task', 'status'],

      properties: {
        id: {type: 'integer'},
        status: {type: 'string', minLength: 1, maxLength: 45},
      },
    };
  }
}

module.exports = Status;
