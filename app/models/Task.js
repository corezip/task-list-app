/* eslint-disable require-jsdoc */
const {Model} = require('objection');
// const User = require('./User');
const File = require('./File');
const Status = require('./Status');

class Tasks extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'tasks';
  };

  static get relationMappings() {
    return {
      files: {
        relation: Model.HasManyRelation,
        modelClass: File,
        join: {
          from: 'tasks.id',
          to: 'files.taskID'},
      },
      status: {
        relation: Model.HasOneRelation,
        modelClass: Status,
        join: {
          from: 'tasks.statusID',
          to: 'status.id'},
      },
    };
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['task'],

      properties: {
        id: {type: 'integer'},
        tasks: {type: 'string', minLength: 1, maxLength: 150},
        description: {type: 'string', minLength: 1, maxLength: 450},
        task_datetime: {type: 'date-time'},
        statusID: {type: 'integer'},
        userID: {type: 'integer'},
      },
    };
  }
}

module.exports = Tasks;
