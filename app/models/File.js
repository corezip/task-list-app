/* eslint-disable require-jsdoc */
const {Model} = require('objection');

class Files extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'files';
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['taskID', 'path'],

      properties: {
        id: {type: 'integer'},
        tasksID: {type: 'integer'},
        path: {type: 'string', minLength: 1, maxLength: 450},
        file_name: {type: 'string', minLength: 1, maxLength: 450},
      },
    };
  }
}

module.exports = Files;
