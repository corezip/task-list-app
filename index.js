
const express = require('express');
const bodyParser = require('body-parser');
const Knex = require('knex');
const {Model} = require('objection');
const isAuth = require('./app/middleware/auth');
const {objectionDb} = require('./app/config/config');
const auth = require('./app/controller/auth');
const users = require('./app/controller/users');
const task = require('./app/controller/task');
const files = require('./app/controller/file');
const ejercicio = require('./app/controller/reto');
const app = express();

// Middleware
app.use(bodyParser.json());

// Routes
app.use('/auth', auth);
app.use('/users', isAuth, users);
app.use('/reto', ejercicio);
app.use('/tasks', isAuth, task);
app.use('/files', files);

// Initialize knex.
// eslint-disable-next-line new-cap
const knex = Knex(objectionDb);
Model.knex(knex);

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});

module.exports = app;
