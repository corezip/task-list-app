const request = require('supertest');
const app = require('../index');

describe('POST /user', () => {
  it('Prueba para el ejercicio', (done) => {
    request(app)
        .post('/reto')
        .send({'array': [1, 2, 3, 5, 6, 8, 10], 'buscame': '5'})
        .set('Accept', 'application/json')
        .expect((res) => {
          res.body.resultado = 3;
        })
        .expect(200, done);
  });
});

describe('POST /user', () => {
  it('Prueba para el ejercicio', (done) => {
    request(app)
        .post('/reto')
        .send({'array': [1, 3, 6, 8, 10], 'buscame': '5'})
        .set('Accept', 'application/json')
        .expect((res) => {
          res.body.resultado = 2;
        })
        .expect(200, done);
  });
});

describe('POST /auth/login', () => {
  it('Prueba Login', (done) => {
    request(app)
        .post('/auth/login')
        .send({'user': 'core', 'password': '123456'})
        .set('Accept', 'application/json')
        .expect((res) => {
          res.body.user.username = 'core';
          res.body.user.id = 2;
        })
        .expect(200, done);
  });
});

after(function(done) {
  process.exit();
});
