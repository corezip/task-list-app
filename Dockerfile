FROM node:11
EXPOSE 3000
WORKDIR /app
ADD . /app/
RUN npm install
CMD ["npm", "start"]
