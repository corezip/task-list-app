#!/bin/sh

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$AWS_CERT" | base64 -d | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
./deploy/disableHostKeyChecking.sh

# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVER_DEV=$AWS_IP

# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
#ALL_SERVERS=(${DEPLOY_SERVER_DEV//,/ })
#echo "ALL_SERVERS ${ALL_SERVERS}"

# Lets iterate over this array and ssh into each EC2 instance
# Once inside.
# 1. Stop the server
# 2. Take a pull
# 3. Start the server
# for server in "${ALL_SERVERS[@]}"
# do
  echo "deploying to ${AWS_IP}"
ssh ubuntu@${AWS_IP} 'bash -s' < ./deploy/updateAndRestart.sh ${PROJECT_NAME} ${PROJECT_PORT} ${DOCKER_USER} ${DOCKER_PASS} ${REGISTRY}
# done
