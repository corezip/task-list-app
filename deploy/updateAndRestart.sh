#!/bin/sh

# any future command that fails will exit the script
set -e

echo "deploying $1 app"

# source /home/ubuntu/.nvm/nvm.sh

# cd bquate-apps/

# cd $1

# echo "git pull"
# git pull

echo "stop and delete container if exist"
docker stop $1-app || true && docker rm $1-app || true

echo "docker login"
docker login $5 -u `echo $3 | base64 -d` -p `echo $4 | base64 -d`

echo "docker pull $5:latest"
docker pull $5:latest

echo "docker image ls"
docker image ls

echo "docker run -d -p $2:$2 --name $1-app $5:latest"
docker run -d -p $2:$2 --name $1-app $5:latest

echo "docker ps"
docker ps

echo "clean docker images"
docker image prune -f